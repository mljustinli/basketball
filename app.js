var express = require("express");
var app = express();
var serv = require("http").Server(app);

var Game = require("./js/game");

app.get("/", function(req, res) {
    res.sendFile(__dirname + "/client/index.html");
});
app.use(express.static(__dirname + "/client"));

serv.listen(process.env.PORT || 3000);

var SOCKET_LIST = {};

var io = require("socket.io")(serv, {});

var currSocket;
var games = [];

io.sockets.on("connection", function(socket) {
    currSocket = socket;
    socket.id = Math.random();
    SOCKET_LIST[socket.id] = socket;
    socket.emit("sendID", socket.id);

    //disconnect event doesn't need emit in html
    socket.on("disconnect", function() {
        // TODO remove player from a game
        games.forEach((game) => {
            game.removePlayer(socket);
        });

        delete SOCKET_LIST[socket.id];
    });

    socket.on("createNewGame", function() {
        // create the game
        let game = new Game();

        // add player to the game
        game.addPlayer(socket, Math.floor(Math.random() * 2) == 0);

        // update list
        games.push(game);
    });

    socket.on("joinGame", function(data) {
        let gameIndex = data;
        if (games.length > 0) {
            games[gameIndex].addPlayer(
                socket,
                Math.floor(Math.random() * 2) == 0
            );
            socket.emit("joined", true);
        }
    });

    // {id, deltaPos}
    socket.on("updatePlayerPos", function(data) {
        if (SOCKET_LIST[data.id].player) {
            SOCKET_LIST[data.id].player.pos.x += data.pos.x;
            SOCKET_LIST[data.id].player.pos.y += data.pos.y;
        }
    });
});

setInterval(function() {
    // update and remove any tracked games if necessary
    for (let index = 0; index < games.length; index++) {
        let currGame = games[index];
        currGame.update();

        // remove the game (when there are no players left)
        if (currGame.remove) {
            games.splice(index, 1);
            index--;
        }
    }
}, 1000 / 30);
