function newGame() {
    if (socket && !inGame) {
        console.log("Creating new game");
        socket.emit("createNewGame");
        inGame = true;
    }
}

function joinGame() {
    // for now just join the first game lol if it exists
    if (!inGame) {
        socket.emit("joinGame", 0);
    }
    // if no space, join as spectator
}

socket.on("hello", function(data) {
    console.log("received message");
});

socket.on("sendID", function(data) {
    id = data;
    console.log("Received a new id: " + id);
});

socket.on("joined", function(data) {
    if (data) {
        inGame = true;
    }
});
