var game;
var id;
var inGame = false;
var isRed;
var currKeys = [];
var PLAYER_SPEED = 2;

function setup() {
    createCanvas(480, 640);
}

function draw() {
    background(255);
    noStroke();
    if (game) {
        if (isRed == null) {
            for (let key in game.players) {
                if (game.players[key].id == id) {
                    isRed = game.players[key].isRed;
                }
            }
        }
        for (let key in game.players) {
            drawPlayer(game.players[key]);
        }
        updatePos();
    }
    fill(0);
    rect(20, 20, 20, 20);
}

function drawPlayer(player) {
    if (player.isRed) {
        fill(255, 0, 0);
    } else {
        fill(0, 0, 255);
    }
    ellipse(player.pos.x, player.pos.y, 20, 20);
}

function updatePos() {
    let deltaPos = { x: 0, y: 0 };
    // 87, 65, 83, 68
    if (currKeys.includes(87)) {
        deltaPos.y -= PLAYER_SPEED;
    }
    if (currKeys.includes(83)) {
        deltaPos.y += PLAYER_SPEED;
    }
    if (currKeys.includes(65)) {
        deltaPos.x -= PLAYER_SPEED;
    }
    if (currKeys.includes(68)) {
        deltaPos.x += PLAYER_SPEED;
    }
    socket.emit("updatePlayerPos", { id: id, pos: deltaPos });
}

function keyPressed() {
    console.log(keyCode);
    currKeys.push(keyCode);
}

function keyReleased() {
    let index = currKeys.indexOf(keyCode);
    if (index > -1) {
        currKeys.splice(index, 1);
    }
}

var socket = io();

socket.on("gameUpdate", function(data) {
    game = data;
});
