/**
 * Holds all information about a basketball game
 */

let Player = require("./player");
let GameData = require("./gameData");

let width = 480;
let height = 640;

// let util = require("util");

let globalID = 0;

class Game {
    constructor() {
        this.id = globalID;
        globalID++;
        this.remove = false; // remove game if there are no playeres left
        this.ready = false;
        this.sockets = {};
        this.gameData = new GameData();
    }

    update() {
        for (let key in this.sockets) {
            this.sockets[key].player.update();
        }
        this.sendUpdate();
    }

    /**
     * Add a player to a game given a socket and whether or not
     * they're on the red team
     */
    addPlayer(socket, isRed) {
        console.log("Adding player: " + socket.id);
        let newPos = {
            x:
                width / 5 +
                (width / 5) *
                    (isRed ? this.gameData.numRed : this.gameData.numBlue),
            y: isRed ? height * (3 / 4.0) : height / 4,
        };
        if (isRed) {
            this.gameData.numRed++;
        } else {
            this.gameData.numBlue++;
        }
        let newPlayer = new Player(socket.id, newPos, isRed);
        this.gameData.players[newPlayer.id] = newPlayer;
        this.sockets[newPlayer.id] = socket;
        socket.player = newPlayer;
    }

    removePlayer(socket) {
        if (this.sockets[socket.id]) {
            if (socket.player.isRed) {
                this.gameData.numRed--;
            } else {
                this.gameData.numBlue--;
            }
            console.log(
                "Players left: " +
                    this.gameData.numRed +
                    " red players and " +
                    this.gameData.numBlue +
                    " blue players."
            );
            delete this.sockets[socket.id];
            delete this.gameData.players[socket.id];
        }
    }

    sendUpdate() {
        for (var key in this.sockets) {
            this.sockets[key].emit("gameUpdate", this.gameData);
        }
    }
}
module.exports = Game;
