class Player {
    constructor(id, pos, isRed) {
        this.id = id;
        this.pos = pos;
        this.isRed = isRed;
        this.angle = 90; // direction the player is facing
    }

    update() {}
}

module.exports = Player;
