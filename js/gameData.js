class GameData {
    constructor() {
        this.numRed = 0;
        this.numBlue = 0;
        this.players = {};
        this.redPoints = 0;
        this.bluePoints = 0;
    }
}

module.exports = GameData;
